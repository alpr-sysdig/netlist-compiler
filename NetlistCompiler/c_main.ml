open C_printer
open Netlist
open Netlist_ast
open Scheduler
open Format
open Unix

(** Main enry point for the compiler, mainly prints the begining and the end of the C code and calls functions of c_printer.ml*)
let number_of_steps = ref 1
let no_pretty_printing_c_code = ref false
let dump = ref false
let output_file = ref "/dev/stdout"
let no_time = ref false
(** Handle errors encountered during compilation and displays human readable errors *)
let handle_eval_errors = function
  |Evaluation_Error(Var_Undef s, id) -> print_string ("Var_Undef " ^ s ^ " in equation for " ^ id); raise Exit
  |Evaluation_Error(Invalid_Argument, id) -> print_string ("Invalid_Argument" ^ " in equation for " ^ id); raise Exit
  |Evaluation_Error(Wrong_Operand_Type, id) -> print_string ("Wrong_Operand_Type" ^ " in equation for  " ^ id); raise Exit
  |Evaluation_Error(Wrong_ROM_Data(id, n), id') -> print_string ("Wrong_ROM/RAM_Data" ^ " for ROM/RAM " ^ id ^ " at line " ^ string_of_int(n) ^ " in equation for" ^ id'); raise Exit
  |Evaluation_Error(ROM_File_Not_Found, id) -> print_string ("ROM_File_Not_Found" ^ "for ROM " ^ id); raise Exit
  |_ -> ()

(** print headers, macros for I/O,  and beginning of main *)
let print_beginning ff =
  let s =
"#include <stdio.h>\n#include <stdlib.h>\n
#define GETBINARY(res, size)   internal_index = 0;internal_c = 0;res = 0;while (internal_index < size){internal_c = getchar ();if (internal_c == '0') res = res << 1;else if (internal_c == '1') res = 1 + (res << 1) ;else{fprintf (stderr, \"Wrong input : too short; Exiting\\n\");exit (1);}internal_index ++;}internal_c = getchar ();if (internal_c != '\\n'){fprintf (stderr, \"Wrong input: too long; Exit\\n\");exit (1);}\n
#define PUTBINARY(var, size, file) for(internal_index = size - 1; internal_index >= 0; internal_index --) fputc(48 + ((var & (1 << internal_index)) != 0), file)\n


#include <time.h>
static inline unsigned int get_time(){
    struct tm tm = {0};
    tm.tm_year = 117;
    time_t t_0 = mktime(&tm);
    time_t current = time(NULL);
    return difftime(current, t_0);
}

int main (int argc, char *argv[]) {\n"
    in fprintf ff "%s" s


(**  write internal variables (may yield a gcc wraning if unused) **)
let print_internal_vars ff =
fprintf ff
"int internal_loop, internal_index;
char internal_c;";
if !dump then fprintf ff "int internal_dump;\nFILE * internal_file;@."

(**write loop (using gloabl var number_of_steps) and print current step*)
let print_loop ff =
fprintf ff
"int n;
if (argc > 1) { n = atoi(argv[1]); } else { n = %d; }
while (n--) {"
(!number_of_steps);
if not (!no_time) then fprintf ff
"unsigned int t = get_time();
ram_reg_data1[1] = t;
ram_reg_data2[1] = t;

printf(\"\\n\\n\");
for (internal_dump = 0; internal_dump < 32; internal_dump ++) {
  printf(\"Reg x%%02d : %%*d\", internal_dump, 6, ram_reg_data1[internal_dump]);
  if (internal_dump %% 4 == 3) printf(\"\\n\"); else printf(\",     \");
} printf(\"\\n\\n\");"



(** Close the loop and main *)
let print_end ff = fprintf ff "@[}@.return 0;@.}@.@]"




(** Main function *)
let main filename =
  let p = read_file filename in
  let folder = Filename.dirname filename in
  let f = open_out !output_file in
  let ff = formatter_of_out_channel f in
  (try
     print_beginning ff;
     declare_variables ff p;
     let regsrams, list_id = create_mems ff p folder in
     print_internal_vars ff;
     if !dump then print_dump ff regsrams list_id;
     print_loop ff;
     print_get_inputs_from_std_in ff p;
     print_loop_body ff p;
     update_regsrams ff regsrams list_id;
     print_output_to_std_out ff p;
     if !dump then print_dump ff regsrams list_id;
     print_end ff;
     close_out f;
    with (Evaluation_Error _) as e -> close_out f; handle_eval_errors e);
  if not (!(no_pretty_printing_c_code)) then (Sys.command ("indent " ^ !output_file)) |> ignore


let _ =
  Arg.parse
    [
      "-n", Arg.Set_int number_of_steps, "Number of steps to simulate";
      "--no-indent", Arg.Set no_pretty_printing_c_code, "whether or not to call ident on the c code";
      "--dump", Arg.Set dump, "dumps the content of the RAM at each cycle";
      "-o", Arg.Set_string output_file, "Name of the output file";
      "--no-time", Arg.Set no_time, "Disable intervention on first reg to set time";
    ]
  main ""


