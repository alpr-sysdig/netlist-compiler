open Netlist_ast
open Graph

exception Combinational_cycle

module SSet = Set.Make(String)

let read_exp eq =
  let extract acc = function
    | Avar a -> a::acc
    | Aconst c -> acc
  in match eq with
    | _, Earg a | _, Enot a -> extract [] a
    | _, Ereg r -> r::[]
    | _, Ebinop (_, a, b) -> extract (extract [] a) b
    | _, Emux (a, b, c) -> extract (extract (extract [] a) b) c
    | _, Eselect (_, a) -> extract [] a
    | _, Eslice (_, _, a) -> extract [] a
    | _, Econcat (a, b) -> extract (extract [] a) b
    | _, Erom(_, _, a) -> extract [] a
    | _, Eram(_, _, ra, we, wa, d) ->
       (* we can read before writing (write happens at the end of the cycle) *)
        extract [] ra

let add_dependency g eq = match eq with
  | i, Ereg r -> ()
  | i, _ -> List.iter (fun y -> add_edge g i y) (read_exp eq)

let schedule p =
  let g = mk_graph () in
  List.iter (function (i, e) -> add_node g i e |> ignore) p.p_eqs;
  List.iter (add_dependency g) p.p_eqs;
  { p_eqs = List.rev (topological g); p_inputs = p.p_inputs; p_outputs = p.p_outputs; p_vars = p.p_vars }
