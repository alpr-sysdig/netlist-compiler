exception Cycle of Netlist_ast.ident
type mark = NotVisited | InProgress | Visited

type ('a,'b) graph =
  { mutable g_nodes : ('a,'b) node list }
and ('a,'b) node = {
  n_label : 'a;
  n_data : 'b;
  mutable n_mark : mark;
  mutable n_link_to : ('a,'b) node list;
  mutable n_linked_by : ('a,'b) node list;
  }

let mk_graph () = { g_nodes = [] }

let add_node g x d =
  let n = { n_label = x; n_data = d; n_mark = NotVisited; n_link_to = []; n_linked_by = [] } in
  g.g_nodes <- n::g.g_nodes;
  n

let node_for_label g x =
  List.find (fun n -> n.n_label = x) g.g_nodes

let add_edge g id1 id2 =
  try
  let n1 = node_for_label g id1 in
  let n2 = node_for_label g id2 in
  n1.n_link_to <- n2::n1.n_link_to;
  n2.n_linked_by <- n1::n2.n_linked_by
  with Not_found -> ()

let clear_marks g =
  List.iter (fun n -> n.n_mark <- NotVisited) g.g_nodes

(* now maybe wrong, warning *)
let find_roots  g =
  List.filter (fun n -> n.n_linked_by = []) g.g_nodes

let has_cycle g =
  clear_marks g;
  let rec visit n = match n.n_mark with
    | InProgress -> raise (Cycle (n.n_label))
    | Visited -> ()
    | NotVisited -> n.n_mark <- InProgress;
                    List.iter visit n.n_link_to;
                    n.n_mark <- Visited
  in try
    List.iter visit g.g_nodes; false
  with Cycle _ -> true

let topological g =
  clear_marks g;
  let rec aux acc n = match n.n_mark with
    | InProgress -> raise (Cycle (n.n_label))
    | Visited -> acc
    | NotVisited -> n.n_mark <- InProgress;
                    let l = List.fold_left aux acc n.n_link_to
                    in n.n_mark <- Visited; (n.n_label, n.n_data)::l
  in List.fold_left aux [] g.g_nodes

