(** Does the bulk of the work *)





(** General remarks :
- No type checking is made at all : it's the caller to ensure types are ok : for instance if x = CONCAT y z we assume that len(x) = len(y) + len(z) (WARNING this may be dangerous, change this ?)
-There is no collisions between variables, rams, roms, regs and internal varibales names thanks to prefixing
-Convention : in memory first wire is first bit (the least important). Thus the value in memory is the same as the one passed in input. But the first bit for I/O (ie the leftmost) is the most significant bit.
If the input is "b_(n-1)b_(n-2)...b_0" then the value in memory is b_0 * 2 ^ 0 + b_1 * 2 ^ 1 + ... + b_(n - 1) ^ 2 ^ (n - 1).
*)


open Format
open Netlist_ast

type loc = ident
(** Describes an error encountered during compilation (name is for legacy reasons) *)
type eval_error = Var_Undef of ident | Wrong_Operand_Type |Invalid_Argument |Wrong_ROM_Data of ident * int |ROM_File_Not_Found |Unsupported_Size of ident * int
exception Evaluation_Error of eval_error *  loc

(** Sizes used to determine which type to use : WARNING may be implementation dependant (ok with my gcc on linux) *)
let size_dic = [(8, "unsigned char"); (16, "unsigned short"); (32, "unsigned int"); (64, "unsigned long long")]

(** Choose the smallest type fit for varibale var in the association list size_dic *)
let choose_type var size =
  let rec aux = function
    |(s, _) :: q when s < size -> aux q
    |(_, t) :: _ -> t
    |[] -> raise (Evaluation_Error(Unsupported_Size(var, size), ""))
  in aux size_dic

let print_value ff = function
  |VBit b -> fprintf ff "%d" (if b then 1 else 0)
  |VBitArray a ->
     let s = String.make (Array.length a) '0' in
     Array.iteri (fun index b -> if b then s.[index] <- '1' else s.[index] <- '0') a;
     fprintf ff "%s%s" "0b" s

let print_arg ff = function
  |Avar id -> fprintf ff "var_%s" id
  |Aconst v -> print_value ff v

(** Computes the length of a value (in bits) *)
let value_length = function
  |(VBit _) -> 1
  |(VBitArray a) -> Array.length a

(** Computes the length of a type (in bits)*)
let type_length = function
  |TBit -> 1
  |TBitArray n -> n

(** Computes the length of an arg (in bits)*)
let arg_length p = function
  |Aconst v -> value_length v
  |Avar id -> Env.find id p.p_vars |> type_length

(** write code to declare variables*)
let declare_variables ff p =
  Env.iter (fun id t -> fprintf ff "@[%s@ var_%s = 0;@]@." (Env.find id p.p_vars |> type_length |> choose_type id) id) p.p_vars

(** Outputs a string corresponding to a mask of length k : ie it will return 2^k - 1 *)
let mask_of k = (string_of_int ((1 lsl k) - 1))


(** Wites binary operator except Nand which is a bit (...) trickier *)
let print_binop_except_nand ff op = fprintf ff (
  match op with
  |Or -> "|"
  |And -> "&"
  |Xor -> "^"
  |_ -> failwith "Not implemented")


(** Writes the code to do a concat : affects all the bits.*)
let print_concat ff p a1 a2 =
  fprintf ff "(%a << %d) + %a" print_arg a1 (arg_length p a2) print_arg a2

(** (name is for legacy reasons), analogue to eval for the interpreter, does the bulk of the work : compiles an exp to C, it is not its responsability to add ";" and flush *)
let eval_exp ff p id e = match e with
  |Earg a -> print_arg ff a
  |Enot a -> fprintf ff "(~ %a) & %s" print_arg a (arg_length p a |> mask_of)
  |Ebinop(Nand, a1, a2) ->  fprintf ff "(~ (%a & %a) & %s)" print_arg a1 print_arg a2 (arg_length p a1 |> mask_of)
  |Ebinop(op, a1, a2) -> print_arg ff a1; print_binop_except_nand ff op; print_arg ff a2;
  |Ereg id -> fprintf ff "reg_%s" id
  |Eram(_,_, read_adress, _,_,_) -> fprintf ff "ram_%s[%a]" id print_arg read_adress
  |Erom(_,_, read_adress) -> fprintf ff "rom_%s[%a]" id print_arg read_adress
  |Emux(a0, a1, a2) -> fprintf ff "%a ? %a  : %a" print_arg a0 print_arg a2 print_arg a1
  |Eselect(i, a) ->
    let j = (arg_length p a - i - 1)
    in fprintf ff "(%a & %d) >> %d" print_arg a (1 lsl j) j
  |Eslice(i1, i2, a) ->
    let j1 = (arg_length p a - i2 - 1) in
    let j2 = (arg_length p a - i1 - 1) in
    fprintf ff "(%a >> %d) & %s" print_arg a j1 (mask_of (j2 - j1 + 1))
  |Econcat(a1, a2) -> print_concat ff p a1 a2

(** Integrity check for ROAM data *)
let check_data_for_roam word_size rid i s =
  if not (String.length s = word_size) then raise (Evaluation_Error(Wrong_ROM_Data(rid, i), rid));
  String.iter (function x -> if not (x = '0' || x = '1') then raise (Evaluation_Error(Wrong_ROM_Data(rid, i), rid))) s


(** Do rom/ram initilization by reading the file in the same folder as the netlist file named id.rom/id.ram. The convention is one line per adress, behaviour is undefined otherwise. If no rom file is provided raises an Evaluation_Error, if no ram file is provided does nothing (we turning indent off as gnu indent has a little pb with binary int). WARNING we don't care if there's too much data*)
let read_roam ff rid is_ram folder addr_size word_size =
  let tdata = choose_type rid word_size in
  let t = if is_ram then "ram" else "rom" in
  try
     let ichannel = open_in (folder ^ Filename.dir_sep ^rid ^ "." ^ t) in
     fprintf ff "  /* *INDENT-OFF* */\n";
     fprintf ff "  %s %s_%s [%d] = {" tdata t rid (1 lsl addr_size);
     for i = 0 to (1 lsl addr_size) - 1 do
       let s = input_line ichannel in
       fprintf ff "0b%s, " s;
     done;
     fprintf ff "};@.";
     fprintf ff "  /* *INDENT-ON* */\n";
   with
   |Sys_error s when  (s = (folder ^ Filename.dir_sep ^rid ^ "." ^ t) ^": No such file or directory") && (not is_ram) -> Evaluation_Error(ROM_File_Not_Found, rid) |> raise
   |Sys_error s when  (s = (folder ^ Filename.dir_sep ^rid ^ "." ^ t) ^": No such file or directory") -> fprintf ff "@[%s ram_%s[%d] = {0};@]@." tdata rid (1 lsl addr_size)
   |e -> raise e


(** Do a first pass on the ast to discover regs, rams and roms, write the intitlization on the file and returns one exp list for containing the expr related to regs and rams for writing their update and the corresponding ident list. We are using a Set for regs as we wish to e able to check if a reg has alkready been created *)
module StrSet = Set.Make(String)

let create_mems ff p folder =
  let (l1, l2, _) = List.fold_left (
      fun (regsrams, list_id, regs_id_set) (eqid, e) ->
      match e with
      |Ereg id when StrSet.mem id regs_id_set -> (regsrams, list_id, regs_id_set)
      |Ereg id -> fprintf ff "@[%s reg_%s = 0;@]@." (Env.find id p.p_vars |> type_length |> choose_type id) id; ((e :: regsrams), eqid :: list_id, StrSet.add id regs_id_set)
      |Eram(addr_size, word_size, _,_,_,_) -> read_roam ff eqid true folder addr_size word_size;  (e :: regsrams), eqid :: list_id, regs_id_set
      |Erom(addr_size, word_size, _) -> read_roam ff eqid false folder addr_size word_size;  regsrams, list_id, regs_id_set
      |_ -> regsrams, list_id, regs_id_set) ([],[], StrSet.empty) p.p_eqs
  in l1, l2
let update_regsrams ff regsrams list_id =
  List.iter2 (
      fun e id ->
      match e with
      |Ereg id -> fprintf ff "@[reg_%s = var_%s;@]@." id id
      |Eram(_, _, _, we, wa, data) -> fprintf ff "@[if (%a) ram_%s[%a] = %a;@]@." print_arg we id print_arg wa print_arg data
      |_ -> failwith "Internal error in the update_regsrams function c_main"
    ) regsrams list_id

let print_dump ff regsrams list_id =
  List.iter2 (
      fun e id ->
      match e with
      |Eram(addr_size, word_size, _,_,_,_) -> fprintf ff "
                                                        internal_file = fopen(\"%s.ram_dump\", \"a\");
                                                        for(internal_dump = 0; internal_dump < %d; internal_dump ++){
                                                          PUTBINARY(ram_%s[internal_dump], %d, internal_file);
                                                          fputc(' ', internal_file);
                                                        }
                                                        fputc('\\n', internal_file);
                                                        fclose(internal_file);" id (1 lsl addr_size) id word_size
      |_ -> ()
) regsrams list_id


(** write c endline with ; *)
let print_c_endline ff = fprintf ff "@[;@.@]"

(** Adds id info to errors encountered *)
let handle_errors id = function
  |Evaluation_Error(reason, _) -> raise (Evaluation_Error(reason, id))
  |e -> raise e
(** write c code corresponding to eq *)
let print_eq ff p (id, e) = try
    fprintf ff "@[var_%s@ =@ @]" id; eval_exp ff p id e; print_c_endline ff
 with ex -> handle_errors id ex


(** write c code to print output to stdout *)
let print_output_to_std_out ff p =
  List.iter (
      function id ->
               let size = Env.find id p.p_vars |> type_length in
               fprintf ff
                       "printf(\"%s = \"); PUTBINARY(var_%s, %d, stdout);putchar('\\n');" id id size
    ) p.p_outputs

(** write c code to ask for input from stdin : warning variables must be declared before *)
let print_get_inputs_from_std_in ff p =
  List.iter (
      function id ->
               let size = arg_length p (Avar id) in
               fprintf ff
                       "puts(\"%s [%d] ? \"); GETBINARY(var_%s, %d);\n" id size id size
    )p.p_inputs

(** write c code corresponding to the main body of the loop*)
let print_loop_body ff p =
  List.iter (print_eq ff p) p.p_eqs

