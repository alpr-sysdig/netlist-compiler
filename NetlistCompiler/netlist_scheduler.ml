let output_file = ref "/dev/stdout"

let compile filename =
  try
    let p = Netlist.read_file filename in
    let out = open_out !output_file in
    let close_all () =
      close_out out
    in
    begin try
        let p = Scheduler.schedule p in
        Netlist_printer.print_program out p;
      with
        | Scheduler.Combinational_cycle ->
            Format.eprintf "The netlist has a combinatory cycle.@.";
            close_all (); exit 2
    end;
    close_all ();
  with
    | Netlist.Parse_error s -> Format.eprintf "An error accurred: %s@." s; exit 2

let main () =
  Arg.parse
    ["-o", Arg.Set_string output_file, "Name of the output file"]
    compile
    ""
;;

main ()
