main:
	cd NetlistCompiler && ocamlbuild netlist_scheduler.native && mv netlist_scheduler.native netlist_scheduler.byte
	cd NetlistCompiler && ocamlbuild c_main.byte
	cd Minijazz && ocamlbuild mjc.byte
	cp NetlistCompiler/netlist_scheduler.byte .
	cp NetlistCompiler/c_main.byte .
	cp Minijazz/mjc.byte .
	

clean:
	rm -rf NetlistCompiler/_build
	rm -f NetlistCompiler/netlist_scheduler.byte
	rm -f NetlistCompiler/c_main.byte
	rm -rf Minijazz/_build
	rm -f Minijazz/mjc.byte
	rm -f netlist_scheduler.byte
	rm -f c_main.byte
	rm -f mjc.byte
 
