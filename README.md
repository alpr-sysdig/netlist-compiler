# Netlist Compiler #
To compile the project run :  
	`make`   
at the root of the project.  
It will creates two files : `c_main.byte`, the Netlist to C compiler and  `mjc.byte`, the Minijazz to Netlist compiler
(Warning: this does not compile with the most recent Ocaml compiler ie 4.06.0, you must have at most the 4.05.0)